function val = IndSmooth(x,sigma)
%INDSMOOTH returns the smoothed approximation of the indicator function [x>0]
%given by (1+exp(-x/sigma))^-1
%   [val] = IndSmooth(x,sigma) returns the value of the smoothed indicator
%   function [x>0] for a vector x and a smoothing constant sigma > 0. The 
%   smaller the sigma, the tighter the approximation.

    % Get x/sigma
    y = x/sigma;

    % Manually set the output to 1 for values of x much smaller than -sigma.
    % We do this to avoid issues with calculating exp(large number)
	pos_ind = y < -10;
    val(pos_ind) = 0;
    
    % Calculate the smoothed function value for |x| < 10*sigma
    smooth_ind = logical(y >= -10);
    val(smooth_ind) = 1./(1+exp(-y(smooth_ind)));
end