function tau = ComputeSEATorques(q,dq,u,du,p)
%COMPUTESEATORQUES computes the SEA joint torques for a model of the robot
%RAMone
%
% Returns the joint torques tau that are generated for a given joint and 
% motor position and velocity at each series elastic actuator in the robot. 
% 
% Input:  - q: The configuration of the robot (7x1 vector)
%         - dq: the configuration velocity of the robot (7x1 vector)
%         - u: The motor positions of the robot (4x1 vector)
%         - du: The motor velocities of the robot (4x1 vector)
%         - p: A structure containing the parameters of the robot. This is
%              gneerated with the function Parameters.m. (struct) 
% Output: - tau: The torques between the motor and joint (4x1 vector) 

%   Details of the model and of the optimization results can be found in 
%   the paper "The Significance of Robotic Gait Selection: A Case 
%   Study on the Robot RAMone" submitted to Robotics and Automation Letters 
%   2016.

%   The remainder of the code can be found at:
%   https://bitbucket.org/ramlab/ral_2016

%   Created by Nils Smit-Anseeuw (1) on 9-9-16
%   MATLAB 2015b

%   (1) Robotics and Motion Laboratory
%       University of Michigan Ann Arbor
%       nilssmit@umich.edu

%   See also CONTINUOUSDYNAMICSCOSTCONSTRAINTS
%% Get SEA deflections and velocities
delta_alpha_L = q(4) - u(1);
ddelta_alpha_L = dq(4) - du(1);

delta_beta_L = q(5) - u(2);
ddelta_beta_L = dq(5) - du(2);

delta_alpha_R = q(6) - u(3);
ddelta_alpha_R = dq(6) - du(3);

delta_beta_R = q(7) - u(4);
ddelta_beta_R = dq(7) - du(4);

%% Compute hip torques
% The hip springs are modelled as a linear spring damper system (see Eq. 1
% in the paper for the governing equation)
tau_alpha_L = -p.k_alpha * delta_alpha_L - p.b_alpha * ddelta_alpha_L;

tau_alpha_R = -p.k_alpha * delta_alpha_R - p.b_alpha * ddelta_alpha_R;

%% Compute knee torques
% The knee series elastic actuators are modelled as a nonlinear spring
% damper system (see Sec. II.2 in the paper for a description and  Eq. 2 
% for the governing equation). 
%
% Note that the functions min(0,x), indicator(x>0) and sign(x) from Eq. 2 
% are all nonsmooth. Since this is problematic for the optimization, we 
% express sign(x) as (2*indicator(x>0) - 1)) and replace min(0,x), and 
% indicator(x>0) with the logistic approximations found in MINSMOOTH.m and 
% INDSMOOTH.m respectively.
tau_beta_L = -p.k_beta1*delta_beta_L - (p.k_beta2 - p.k_beta1) * MinSmooth(delta_beta_L - p.beta_sm, p.sigma) ...
             -p.b_beta2*ddelta_beta_L - (p.b_beta1 - p.b_beta2)*ddelta_beta_L * IndSmooth(delta_beta_L - p.beta_sm, p.sigma) ...
                                                                              * IndSmooth(ddelta_beta_L, p.sigma) ...
             -p.f_beta * (2*IndSmooth(ddelta_beta_L, p.sigma) - 1);

tau_beta_R = -p.k_beta1*delta_beta_R - (p.k_beta2 - p.k_beta1) * MinSmooth(delta_beta_R - p.beta_sm, p.sigma) ...
             -p.b_beta2*ddelta_beta_R - (p.b_beta1 - p.b_beta2)*ddelta_beta_R * IndSmooth(delta_beta_R - p.beta_sm, p.sigma) ...
                                                                              * IndSmooth(ddelta_beta_R, p.sigma) ...
             -p.f_beta * (2*IndSmooth(ddelta_beta_R, p.sigma) - 1);

%% Compile the torques into a vector
tau = [tau_alpha_L;
       tau_beta_L;
       tau_alpha_R;
       tau_beta_R];

end

